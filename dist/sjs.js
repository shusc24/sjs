"use strict";
exports.__esModule = true;
var _ = require("lodash");
var TokenType;
(function (TokenType) {
    TokenType[TokenType["normal"] = 0] = "normal";
    TokenType[TokenType["showVariate"] = 1] = "showVariate";
    TokenType[TokenType["expression"] = 2] = "expression";
})(TokenType || (TokenType = {}));
var sjs = /** @class */ (function () {
    function sjs() {
    }
    /**
     *  @param {string} html html文本
     *  @returns {string[]}
     *  @private
     */
    sjs.getHtmlToken = function (html) {
        var htmlToken = [
            {
                type: TokenType.normal,
                text: ""
            }
        ];
        var htmlArr = Array.from(html);
        //遍历字符串 分割
        htmlArr.forEach(function (key, index) {
            var nextKey = _.get(htmlArr, [index + 1], "");
            var token = key + nextKey;
            var prevToken = index >= 2 ? "" + htmlArr[index - 2] + htmlArr[index - 1] : "";
            //根据token 来进行分割
            switch (token) {
                case sjs._flag.close:
                    htmlToken.push({
                        type: TokenType.normal,
                        text: ""
                    });
                    break;
                case sjs._flag.start:
                    //操作符号
                    var operationFlag = _.get(htmlArr, [index + 2], "");
                    switch (operationFlag) {
                        case "=":
                            htmlToken.push({
                                type: TokenType.showVariate,
                                text: ""
                            });
                            break;
                        case " ":
                            htmlToken.push({
                                type: TokenType.expression,
                                text: ""
                            });
                            break;
                    }
                    break;
            }
            var currentHtmlArr = _.last(htmlToken);
            if (prevToken === sjs._flag.close) {
                var currentArrIndex = htmlToken.length - 2 >= 0 ? htmlToken.length - 2 : htmlToken.length - 1;
                //防止错误
                htmlToken[currentArrIndex].text += "%>";
                //去掉头部
                currentHtmlArr.text = "";
            }
            currentHtmlArr.text += key;
        });
        return htmlToken;
    };
    sjs.createRenderFn = function (tokens, data) {
        var paramsName = (function () {
            var paramsKeys = _.map(data, function (val, keyname) {
                return keyname;
            });
            if (paramsKeys.length > 0) {
                return paramsKeys.join(",");
            }
            else {
                return "";
            }
        })();
        var fnBody = tokens.map(function (token) {
            switch (token.type) {
                case TokenType.normal:
                    return "__html +=" + "`" + token.text + "`;";
                case TokenType.expression:
                    return token.text.replace(sjs._flag.matchReg, "$1") + ";";
                case TokenType.showVariate:
                    var variate = token.text.replace(sjs._flag.matchReg, "$1");
                    return "__html += " + variate + ";";
            }
        }).join("");
        var fn = "\n                (function(){\n                   return function({" + paramsName + "}){\n                       let __html = \"\";\n                       " + fnBody + "\n                       return __html;\n                   } \n                })()\n           ";
        return eval(fn);
    };
    sjs.compile = function (str, options) {
        var _options = options;
        return function (data) {
            return sjs.render(str, data, _options);
        };
    };
    sjs.render = function (str, data, options) {
        var tokens = sjs.getHtmlToken(str);
        var renderFn = sjs.createRenderFn(tokens, data);
        return renderFn(data);
    };
    sjs.renderFile = function (filename, data, options, fn) {
    };
    sjs._flag = {
        start: "<%",
        close: "%>",
        matchReg: /<%[=\-_]? (.*)%>/g
    };
    return sjs;
}());
module.exports = sjs;
//# sourceMappingURL=sjs.js.map